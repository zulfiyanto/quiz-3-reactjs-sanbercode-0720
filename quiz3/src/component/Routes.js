import React from "react";
import { Switch, Route } from "react-router-dom";
import Heading from "./Heading";
import Home from "./Home";
import About from "./About";
import MovieListEditor from "./MovieListEditor";

export default function App() {
  return (
    <>
      <Heading />
      <Switch>
        <Route path="/">
          <Home />
        </Route>
        <Route path="">
          <About />
        </Route>
        <Route path="">
          <MovieListEditor />
        </Route>
      </Switch>
    </>
  );
}
