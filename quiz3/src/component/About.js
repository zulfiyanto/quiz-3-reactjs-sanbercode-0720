import React from "react";

const About = () => {
  return (
    <body>
      <div className="container" style={{ borderRight: "3px solid black" }}>
        <div className="box">
          <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
          <ol>
            <li>
              <b>Nama: </b>Zulfikar Fitri Istyanto
            </li>
            <li>
              <b>Email: </b>zulfikar.zfi@gmail.com
            </li>
            <li>
              <b>Sistem Operasi yang digunakan: </b>Windows 10
            </li>
            <li>
              <b>Akun Gitlab: </b>https://gitlab.com/zulfiyanto
            </li>
            <li>
              <b>Akun Telegram: </b>@Zulfiyanto
            </li>
          </ol>
        </div>
      </div>
      <a href="index.html">Kembali Ke Index</a>
    </body>
  );
};

export default About;
