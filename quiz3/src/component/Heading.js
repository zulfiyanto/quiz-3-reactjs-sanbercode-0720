import React from "react";
import Logo from "./img/logo.png";
import { Link } from "react-router-dom";
const Heading = () => {
  return (
    <header>
      <div className="logo-container">
        <img src={Logo} width="200px" alt="pict" />
      </div>
      <nav>
        <ul className="nav-links">
          <li>
            <Link to="">Home</Link>
          </li>
          <li>
            <Link to="About">About</Link>
          </li>
          <li>
            <Link to="MovieListEditor">Movie List Editor</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Heading;
